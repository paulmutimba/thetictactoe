#pragma once
#include <iostream>
class TicTacToe
{
private:
	int m_numTurns;
	char m_playerTurn;
	char m_winner;
	char m_board[9] = { ' ',' ',' ',' ',' ',' ',' ',' ',' ' };
public:
	TicTacToe() {
		m_numTurns = 0;
		m_winner = ' ';
		m_playerTurn = 'x';
	};
	void DisplayBoard() { //Displays positions 1 - 9
		for (int i = 0; i < 9; i++) {
			if ((i == 2) || (i == 5)) {
				std::cout << m_board[i] << "\n";
				std::cout << "---|----|---\n";
				continue;
			}
			if (i == 8) {
				std::cout << "" << m_board[i] << "\n";
				break;
			}
			std::cout << " " << m_board[i] << " | ";

		}
	}
	bool IsOver() { // Shows if the game is over or not

		if (m_numTurns != 0) {// Checks a move
			if (m_numTurns == 9) {
				return true;
			}
			for (int i = 0; i < 8; i = i + 3) { // Marks the rows
				if ((m_board[i] == m_playerTurn) && (m_board[i + 1] == m_playerTurn) && (m_board[i + 2] == m_playerTurn)) {
					m_winner = m_playerTurn;
					return true;
				}
			};
			for (int i = 0; i < 3; i++) { // Marks the columns
				if ((m_board[i] == m_playerTurn) && (m_board[i + 3] == m_playerTurn) && (m_board[i + 6] == m_playerTurn)) {
					m_winner = m_playerTurn;
					return true;
				}
			};
			if ((m_board[0] == m_playerTurn) && (m_board[4] == m_playerTurn) && (m_board[8] == m_playerTurn)) { // top left to bottom right
				m_winner = m_playerTurn;
				return true;
			}
			if ((m_board[2] == m_playerTurn) && (m_board[4] == m_playerTurn) && (m_board[6] == m_playerTurn)) { //top right to bottom left
				m_winner = m_playerTurn;
				return true;
			}



			if (m_playerTurn == 'x') { m_playerTurn = 'o'; }
			else { m_playerTurn = 'x'; } // switches the player after checking if they won
			return false; // assumes all tests failed and will default to false
		}
		else
		{
			return false;
		} // will always assume the game can't be done on the first turn, will always return false (was needed with old code, may be pointless now)
	}
	char GetPlayerTurn() {
		return m_playerTurn;
	}
	bool IsValidMove(int position) {
		if (m_board[(position - 1)] == ' ') { // checks if spot is empty
			return true;
		}
		else {
			return false;
		}
	}
	void Move(int position) {
		m_board[position - 1] = m_playerTurn; // Fills Empty spot with player's symbol
		m_numTurns++;
	}
	void DisplayResult() {
		if ((m_numTurns == 9) && (m_winner == ' ')) { // Assumes is tie if IsOver hasn't called a winner and we are at turn 9
			std::cout << "Tie!\n";
		}
		else {
			std::cout << m_winner << " won!\n";
		}
	}
};